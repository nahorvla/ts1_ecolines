package cz.cvut.fel.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SeleniumUtils {

    public static boolean waitForNameDisplaying(WebDriver driver, By locator, int seconds) {
        try {
            waitForElementVisible(driver, locator, seconds);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isDisplayed(WebDriver driver, By by) {
        try {
            return driver.findElement(by).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static void switchToIframe(WebDriver driver, String iframeId) {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(driver.findElement(By.id(iframeId)));
    }

    public static void scrollToElement(WebDriver driver, By elementBy) {
        WebElement element = driver.findElement(elementBy);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static void waitForElementClickable(WebDriver driver, By elementBy, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.elementToBeClickable(elementBy));
        new Actions(driver).moveToElement(driver.findElement(elementBy)).perform();
    }

    public static void waitForElementVisible(WebDriver driver, By elementBy, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(seconds));
        wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));
    }

    public static void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }
}
