package cz.cvut.fel.pages;

import cz.cvut.fel.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogInPage {
    private final WebDriver driver;

    public LogInPage(WebDriver driver) {
        this.driver = driver;
    }

    private final By emailInput = By.id("email");
    private final By passwordInput = By.id("password");
    private final By loginButton = By.cssSelector(".submit-form-button");

    public void fillEmail(String mail) {
        SeleniumUtils.waitForElementVisible(driver, emailInput, 10);
        WebElement emailElement = driver.findElement(emailInput);
        emailElement.sendKeys(mail);
    }

    public void fillPassword(String pass) {
        SeleniumUtils.waitForElementVisible(driver, passwordInput, 10);
        WebElement passwordElement = driver.findElement(passwordInput);
        passwordElement.sendKeys(pass);
    }

    public void clickSubmitButton() {
        SeleniumUtils.scrollToElement(driver, loginButton);
        SeleniumUtils.waitForElementClickable(driver, loginButton, 10);
        driver.findElement(loginButton).click();
    }

    public void logInProcess(String mail, String password){
        SeleniumUtils.switchToIframe(driver, "iframe");

        fillEmail(mail);
        fillPassword(password);
        clickSubmitButton();
    }
}
