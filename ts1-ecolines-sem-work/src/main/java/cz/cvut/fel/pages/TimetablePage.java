package cz.cvut.fel.pages;

import cz.cvut.fel.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TimetablePage {
    private final WebDriver driver;

    public TimetablePage(WebDriver driver) {
        this.driver = driver;
    }

    private final By fromButton = By.cssSelector("button._button_1hese_31:nth-child(1)");
    private final By toButton = By.cssSelector("button._button_1hese_31:nth-child(2)");

    private final By routesButton = By.cssSelector("div._option_18zj8_32");
    private final By submitSearchButton = By.cssSelector("button._component_c5381_13:nth-child(1)");

    private final By title = By.cssSelector("#schedule");

    public void selectTimetableFromAlytusToLida() {
        SeleniumUtils.waitForElementVisible(driver,fromButton, 10);

        driver.findElement(fromButton).click();
        MainPage.selectOptionByText(driver, routesButton, "Alytus");

        SeleniumUtils.sleep(1500);

        driver.findElement(toButton).click();
        MainPage.selectOptionByText(this.driver, routesButton, "Lida");

        driver.findElement(submitSearchButton).click();
    }

    public String getTitleText() {
        return driver.findElement(title).getText();
    }
}
