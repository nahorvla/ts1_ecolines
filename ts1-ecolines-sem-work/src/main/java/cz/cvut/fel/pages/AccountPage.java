package cz.cvut.fel.pages;

import cz.cvut.fel.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountPage {
    WebDriver driver;

    public AccountPage(WebDriver driver) {
        this.driver = driver;
    }

    public static final By profileHeader = By.cssSelector(".edit-profile-title");
    public static final By profileName = By.id("name");

    private final By changeButton = By.id("save");
    private final By textArea = By.cssSelector("#dscr");
    private final By saveChangeButton = By.cssSelector("#save");

    public void doChangesInUserInfo(String message) {
        SeleniumUtils.waitForElementVisible(driver, changeButton, 10);
        driver.findElement(changeButton).click();
        SeleniumUtils.scrollToElement(driver, textArea);
        driver.findElement(textArea).clear();
        driver.findElement(textArea).sendKeys(message);
        SeleniumUtils.scrollToElement(driver, saveChangeButton);
        driver.findElement(saveChangeButton).click();
    }

    public String getUserName() {
        return driver.findElement(profileName).getText();
    }

    public String getMessageText() {
        return driver.findElement(textArea).getText();
    }
}
