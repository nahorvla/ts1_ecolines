package cz.cvut.fel.pages;

import cz.cvut.fel.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class BusStopsPage {
    private final WebDriver driver;

    public BusStopsPage(WebDriver driver) {
        this.driver = driver;
    }

    private final By firstBelarusCardTitle = By.cssSelector("div._dividerLine_1g3ae_49:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(1) > div:nth-child(1) > div:nth-child(1)");

    private final By cards = By.cssSelector("div._gridContent_1g3ae_13 > div");
    private final By cardTitles = By.cssSelector("div:nth-child(1) > span:nth-child(1)");

    public static final By cardAddresses = By.cssSelector("._gridContent_1g3ae_13");

    public void clickBelarusCardTitle() {
        SeleniumUtils.waitForElementVisible(driver, firstBelarusCardTitle, 10);
        driver.findElement(firstBelarusCardTitle).click();
    }

    public List<String> getCardTitles() {
        SeleniumUtils.waitForElementVisible(driver, cards, 10);
        List<WebElement> cardElements = driver.findElements(cards);
        return cardElements.stream()
                .map(card -> card.findElement(cardTitles).getText())
                .collect(Collectors.toList());
    }

    public boolean verifyCardTitles(List<String> expectedTitles) {
        List<String> actualTitles = getCardTitles();
        return new HashSet<>(expectedTitles).containsAll(actualTitles) && new HashSet<>(actualTitles).containsAll(expectedTitles);
    }
}
