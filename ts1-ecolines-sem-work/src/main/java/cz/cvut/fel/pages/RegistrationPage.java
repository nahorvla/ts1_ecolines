package cz.cvut.fel.pages;

import cz.cvut.fel.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegistrationPage {
    private final WebDriver driver;

    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
    }

    private final By nameInput = By.id("f_name");
    private final By surnameInput = By.id("l_name");
    private final By emailInput = By.id("email");
    private final By passwordInput = By.id("password");
    private final By passwordConfirmInput = By.id("password_confirm");
    private final By dayOfBthInput = By.id("dob_dayMsiSelect");
    private final By monthOfBthInput = By.id("dob_monthMsiSelect");
    private final By agreeWithRulesButton = By.cssSelector(".agree-to-rules");
    private final By submitButton = By.id("save");

    public void fillName(String name) {
        SeleniumUtils.waitForElementVisible(driver, nameInput, 10);
        SeleniumUtils.scrollToElement(driver, nameInput);
        WebElement nameElement = driver.findElement(nameInput);
        nameElement.clear();
        nameElement.sendKeys(name);
    }

    public void fillSurname(String surname) {
        SeleniumUtils.waitForElementVisible(driver, surnameInput, 10);
        SeleniumUtils.scrollToElement(driver, surnameInput);
        WebElement surnameElement = driver.findElement(surnameInput);
        surnameElement.clear();
        surnameElement.sendKeys(surname);
    }

    public void fillEmail(String email) {
        SeleniumUtils.waitForElementVisible(driver, surnameInput, 10);
        SeleniumUtils.scrollToElement(driver, emailInput);
        WebElement emailElement = driver.findElement(emailInput);
        emailElement.clear();
        emailElement.sendKeys(email);
    }

    public void fillPassword(String password) {
        SeleniumUtils.waitForElementVisible(driver, passwordInput, 10);
        SeleniumUtils.scrollToElement(driver, passwordInput);
        WebElement passwordElement = driver.findElement(passwordInput);
        passwordElement.clear();
        passwordElement.sendKeys(password);
    }

    public void fillPasswordConfirm(String passwordConfirm) {
        SeleniumUtils.waitForElementVisible(driver, passwordConfirmInput, 10);
        SeleniumUtils.scrollToElement(driver, passwordConfirmInput);
        WebElement passwordConfirmElement = driver.findElement(passwordConfirmInput);
        passwordConfirmElement.clear();
        passwordConfirmElement.sendKeys(passwordConfirm);
    }

    public void selectDayOfBirth(String dayOfBth) {
        SeleniumUtils.scrollToElement(driver, dayOfBthInput);
        driver.findElement(dayOfBthInput).click();
        for (int i = 0; i <= 30; i++) {
            WebElement element = driver.findElements(By.cssSelector(".msi-select-item")).get(i);
            if (element.getText().equals(dayOfBth)) {
                element.click();
                break;
            }
        }
    }

    public void selectMonthOfBirth(String monthOfBth) {
        SeleniumUtils.scrollToElement(driver, monthOfBthInput);
        driver.findElement(monthOfBthInput).click();
        for (int j = 31; j <= 42; j++) {
            WebElement element = driver.findElements(By.cssSelector(".msi-select-item")).get(j);
            if (element.getText().equals(monthOfBth)) {
                element.click();
                break;
            }
        }
    }

    public void clickAgreeWithRules() {
        SeleniumUtils.scrollToElement(driver, agreeWithRulesButton);
        SeleniumUtils.waitForElementClickable(driver, agreeWithRulesButton, 10);
        driver.findElement(agreeWithRulesButton).click();
    }

    public void clickSubmitButton() {
        SeleniumUtils.scrollToElement(driver, submitButton);
        SeleniumUtils.waitForElementClickable(driver, submitButton, 10);
        driver.findElement(submitButton).click();
    }

    public void registrationProcess(String name, String surname, String email, String password, String passwordConfirm, String dayOfBth, String monthOfBth) {
        SeleniumUtils.switchToIframe(driver, "iframe");

        fillName(name);
        fillSurname(surname);
        fillEmail(email);
        fillPassword(password);
        fillPasswordConfirm(passwordConfirm);
        selectDayOfBirth(dayOfBth);
        selectMonthOfBirth(monthOfBth);
        clickAgreeWithRules();
        clickSubmitButton();
    }
}
