package cz.cvut.fel.pages;

import cz.cvut.fel.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RoutePage {
    private final WebDriver driver;

    public RoutePage(WebDriver driver) {
        this.driver = driver;
    }

    private final By departureButton = By.cssSelector("button._button_16zqw_16:nth-child(1)");
    private final By lastMonthsDateButton = By.cssSelector("div.vc-day:nth-child(42) > span:nth-child(1)");
    private final By findTicketsButton = By.cssSelector("button._component_c5381_13");

    public void findRouteTickets() {
        SeleniumUtils.waitForElementVisible(driver, departureButton, 10);

        driver.findElement(departureButton).click();
        driver.findElement(lastMonthsDateButton).click();

        driver.findElement(findTicketsButton).click();
    }
}
