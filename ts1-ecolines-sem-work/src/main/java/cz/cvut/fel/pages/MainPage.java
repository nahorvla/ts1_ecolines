package cz.cvut.fel.pages;

import cz.cvut.fel.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;


public class MainPage {
    private final WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    private final By loyaltyProgramButton = By.cssSelector("div._item_29p60_24:nth-child(3) > a:nth-child(1) > span:nth-child(2)");
    private final By changeLanguageButton = By.cssSelector("._button_hmu2g_23");

    private final By countryButton = By.cssSelector("div._field_hmu2g_41:nth-child(1) > div:nth-child(2) > button:nth-child(1)");
    private final By countryOptions = By.cssSelector("div._option_14y5s_132");

    private final By languageButton = By.cssSelector("div._field_hmu2g_41:nth-child(2) > div:nth-child(2) > button:nth-child(1)");
    private final By languageOptions = By.cssSelector("._option_14y5s_132");

    private final By currencyButton = By.cssSelector("div._field_hmu2g_41:nth-child(3) > div:nth-child(2) > button:nth-child(1)");
    private final By currencyOptions = By.cssSelector("div._option_14y5s_132");

    private final By saveLanguageButton = By.cssSelector("._controls_hmu2g_51 > button:nth-child(1)");

    private final By fromButton = By.cssSelector("button._button_1hese_31:nth-child(1)");
    private final By routesButton = By.cssSelector("div._option_18zj8_32");

    private final By toButton = By.cssSelector("button._button_1hese_31:nth-child(2)");

    private final By departureButton = By.cssSelector("button._button_16zqw_16:nth-child(1)");
    private final By lastMonthsDateButton = By.cssSelector("div.vc-day:nth-child(42) > span:nth-child(1)");

    private final By submitSearchButton = By.cssSelector("button._component_c5381_13:nth-child(1)");

    private final By navigationMainPage = By.cssSelector("._logo_1ut7y_37");
    private final By navigationOffersPage = By.cssSelector("div._item_1joj2_38:nth-child(1) > a:nth-child(1)");
    private final By navigationBuyTicketPage = By.cssSelector("div._item_1joj2_38:nth-child(2) > a:nth-child(1)");
    private final By navigationMyBookingPage = By.cssSelector("div._item_1joj2_38:nth-child(3) > a:nth-child(1)");
    private final By openInfoDropdownButton = By.cssSelector("div._item_1joj2_38:nth-child(4) > a:nth-child(1)");
    public static final By infoDropdown = By.cssSelector("._dropdown_1joj2_97 > div:nth-child(1)");


    public void clickLoyaltyProgramButton() {
        SeleniumUtils.scrollToElement(driver, loyaltyProgramButton);
        SeleniumUtils.waitForElementClickable(driver, loyaltyProgramButton, 10);
        driver.findElement(loyaltyProgramButton).click();
    }

    public void clickChangeLanguageButton() {
        SeleniumUtils.waitForElementVisible(driver, changeLanguageButton, 10);
        SeleniumUtils.waitForElementClickable(driver, changeLanguageButton, 10);
        driver.findElement(changeLanguageButton).click();
    }

    public void clickBulgarianCountryLanguageAndCurrency() {
        SeleniumUtils.waitForElementVisible(driver, countryButton, 10);

        driver.findElement(countryButton).click();
        selectOptionByText(this.driver, countryOptions, "Bulgaria (България)");

        driver.findElement(languageButton).click();
        selectOptionByText(this.driver, languageOptions, "Bulgarska");

        driver.findElement(currencyButton).click();
        selectOptionByText(this.driver, currencyOptions, "BGN");

        driver.findElement(saveLanguageButton).click();
    }

    public void searchRouteFromAlytusToLida() {
        SeleniumUtils.waitForElementVisible(driver, fromButton, 10);

        driver.findElement(fromButton).click();
        selectOptionByText(this.driver, routesButton, "Alytus");

        SeleniumUtils.sleep(1500);

        driver.findElement(toButton).click();
        selectOptionByText(this.driver, routesButton, "Lida");

        driver.findElement(departureButton).click();
        driver.findElement(lastMonthsDateButton).click();

        driver.findElement(submitSearchButton).click();
    }

    public void clickNavigationMainPage() {
        SeleniumUtils.waitForElementClickable(driver, navigationMainPage, 10);
        driver.findElement(navigationMainPage).click();
    }

    public void clickNavigationOffersPage() {
        SeleniumUtils.waitForElementClickable(driver, navigationOffersPage, 10);
        driver.findElement(navigationOffersPage).click();
    }

    public void clickNavigationBuyTicketPage() {
        SeleniumUtils.waitForElementClickable(driver, navigationBuyTicketPage, 10);
        driver.findElement(navigationBuyTicketPage).click();
    }

    public void clickNavigationMyBookingPage() {
        SeleniumUtils.waitForElementClickable(driver, navigationMyBookingPage, 10);
        driver.findElement(navigationMyBookingPage).click();
    }

    public void clickOpenInfoDropdownButton() {
        SeleniumUtils.waitForElementClickable(driver, openInfoDropdownButton, 10);
        driver.findElement(openInfoDropdownButton).click();
    }

    public static void selectOptionByText(WebDriver driver, By locator, String text) {
        List<WebElement> options = driver.findElements(locator);
        for (WebElement option : options) {
            if (option.getText().contains(text)) {
                option.click();
                break;
            }
        }
    }
}
