package cz.cvut.fel.pages;

import cz.cvut.fel.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoyaltyProgramPage {
    private final WebDriver driver;

    public LoyaltyProgramPage(WebDriver driver) {
        this.driver = driver;
    }

    private final By logInButton = By.xpath("/html/body/div/div/div[2]/div/div[2]/div[8]/div/div/div/a");
    private final By registrationCompanyButton = By.xpath("/html/body/div/div/div[2]/div/div[2]/div[7]/div/div/div/a");
    private final By registrationButton = By.xpath("/html/body/div/div/div[2]/div/div[2]/div[6]/div/div/div/a");

    public void clickLogInButton() {
        SeleniumUtils.scrollToElement(driver, logInButton);
        driver.findElement(logInButton).click();
    }

    public void clickRegistrationCompanyButton() {
        SeleniumUtils.scrollToElement(driver, registrationCompanyButton);
        driver.findElement(registrationCompanyButton).click();
    }

    public void clickRegistrationButton() {
        SeleniumUtils.scrollToElement(driver, registrationButton);
        driver.findElement(registrationButton).click();
    }
}
