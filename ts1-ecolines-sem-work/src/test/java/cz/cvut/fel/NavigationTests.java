package cz.cvut.fel;

import cz.cvut.fel.pages.LoyaltyProgramPage;
import cz.cvut.fel.pages.MainPage;
import cz.cvut.fel.utils.SeleniumUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NavigationTests extends SeleniumBaseTest {

    @Test
    public void navigationPanelTestSelenium() {
        webDriver.get(BASE_URL);

        SeleniumUtils.sleep(2000);

        String expectingOffersURL = BASE_URL + "offers";
        String expectingBuyTicketURL = BASE_URL + "tickets";
        String expectingMyBookingURL = BASE_URL + "tickets/manage";

        MainPage mainPage = new MainPage(webDriver);

        mainPage.clickNavigationMainPage();
        Assertions.assertEquals(BASE_URL, webDriver.getCurrentUrl());

        mainPage.clickNavigationOffersPage();
        Assertions.assertEquals(expectingOffersURL, webDriver.getCurrentUrl());
        webDriver.navigate().back();

        mainPage.clickNavigationBuyTicketPage();
        Assertions.assertEquals(expectingBuyTicketURL, webDriver.getCurrentUrl());
        webDriver.navigate().back();

        mainPage.clickNavigationMyBookingPage();
        Assertions.assertEquals(expectingMyBookingURL, webDriver.getCurrentUrl());
        webDriver.navigate().back();

        SeleniumUtils.sleep(1000);
        mainPage.clickOpenInfoDropdownButton();
        Assertions.assertTrue(SeleniumUtils.isDisplayed(webDriver, MainPage.infoDropdown));
    }

    @Test
    public void testPageTransition() {
        webDriver.get(BASE_URL);

        String expLogURL = BASE_URL + "loyalty-system/login";
        String expRegURL = BASE_URL + "loyalty-system/registration";
        String expCompanyRegURL = BASE_URL + "loyalty-system/business-registration";
        MainPage mainPage = new MainPage(webDriver);
        LoyaltyProgramPage loyaltySystemPage = new LoyaltyProgramPage(webDriver);

        mainPage.clickLoyaltyProgramButton();
        loyaltySystemPage.clickLogInButton();

        Assertions.assertEquals(expLogURL, webDriver.getCurrentUrl());
        webDriver.navigate().back();

        loyaltySystemPage.clickRegistrationCompanyButton();
        Assertions.assertEquals(expCompanyRegURL, webDriver.getCurrentUrl());
        webDriver.navigate().back();

        loyaltySystemPage.clickRegistrationButton();
        Assertions.assertEquals(expRegURL, webDriver.getCurrentUrl());
    }
}
