package cz.cvut.fel;

import cz.cvut.fel.pages.*;
import cz.cvut.fel.utils.SeleniumUtils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

public class AuthAndUserTests extends SeleniumBaseTest {
    private static final Random random = new Random();

    @ParameterizedTest(name = "Does the user have account with a mail {0} and password {1} - {2}")
    @CsvSource({
            "testvladnah@test.com,rN'i~*wAc92wZq~,true",
            "dhsjmds@mail.com,dskhmgfHvhkmd,false",
            "dkhjsfh@gmail.com,pcTPfDbEPCjT2hp,true",
            "fkshfks@sah.ss,pcTPfDbEPCjT2hp,false"
    })
    public void loginTestSeleniumCSV(String email, String password, boolean expectedResult) {
        webDriver.get(BASE_URL);

        MainPage mainPage = new MainPage(webDriver);
        LoyaltyProgramPage loyaltyProgramPage = new LoyaltyProgramPage(webDriver);
        LogInPage logInPage = new LogInPage(webDriver);

        mainPage.clickLoyaltyProgramButton();
        loyaltyProgramPage.clickLogInButton();
        logInPage.logInProcess(email, password);

        boolean tempResult = SeleniumUtils.isDisplayed(webDriver, AccountPage.profileHeader);
        Assertions.assertEquals(expectedResult, tempResult);
    }

    @ParameterizedTest(name = "Create account with {0} name {1} surname {2} mail {3} pass {4} day {5} month  - RESULT name in acc {6}")
    @MethodSource("provideRegistrationData")
    public void registrationTestSeleniumMethodDataProvider(String name, String surname, String email, String password, String passwordConfirm, String day, String month, String expectedResult) {
        webDriver.get(BASE_URL);

        MainPage mainPage = new MainPage(webDriver);
        LoyaltyProgramPage loyaltyProgramPage = new LoyaltyProgramPage(webDriver);
        RegistrationPage registrationPage = new RegistrationPage(webDriver);
        AccountPage accountPage = new AccountPage(webDriver);

        mainPage.clickLoyaltyProgramButton();
        loyaltyProgramPage.clickRegistrationButton();
        registrationPage.registrationProcess(name, surname, email, password, passwordConfirm, day, month);

        boolean waitingResult = SeleniumUtils.waitForNameDisplaying(webDriver, AccountPage.profileName, 3);
        String result;

        if (waitingResult) {
            result = SeleniumUtils
                    .isDisplayed(webDriver, AccountPage.profileName) ? accountPage.getUserName() : "ERROR";
        } else {
            result = "ERROR";
        }

        Assertions.assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @CsvSource({"alala,true",
            "alallkljljjl,true",
            "sorry, dear ecolines, its my coursework :),true"})
    public void changeAccountInfoTestSeleniumCSV(String message, boolean expectedResult) {
        String email = "testvladnah@test.com";
        String password = "rN'i~*wAc92wZq~";

        webDriver.get(BASE_URL + "loyalty-system/login");

        LogInPage logInPage = new LogInPage(webDriver);
        AccountPage accountPage = new AccountPage(webDriver);

        logInPage.logInProcess(email, password);
        SeleniumUtils.waitForElementVisible(webDriver, AccountPage.profileHeader, 10);

        accountPage.doChangesInUserInfo(message);

        boolean result = message.equals(accountPage.getMessageText());
        Assertions.assertEquals(expectedResult, result);
    }

    private static Stream<Arguments> provideRegistrationData() {
        return Stream.of(
                Arguments.of("TestVladyslav", "TestNahorniuk", "testvladnah@test.com", "rN'i~*wAc92wZq~", "rN'i~*wAc92wZq~", "1", "march", "ERROR"),
                Arguments.of("Test", "Testacj", generateRandomEmail(), "pcTPfDfssfsfs", "pcTPfDfssfsfs", "2", "april", "Test Testacj"),
                Arguments.of("Test", "Test", generateRandomEmail(), "pcTPfDfssfsfs", "pcTPfDfssfsdadasd3521fs", "2", "april", "ERROR")
        );
    }

    private static String generateRandomEmail() {
        return generateRandomString() + "@test.com";
    }

    private static String generateRandomString() {
        StringBuilder builder = new StringBuilder(10);
        for (int i = 0; i < 10; i++) {
            String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            builder.append(chars.charAt(random.nextInt(chars.length())));
        }
        return builder.toString();
    }
}
