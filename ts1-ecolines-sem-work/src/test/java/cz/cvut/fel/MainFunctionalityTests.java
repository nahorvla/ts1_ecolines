package cz.cvut.fel;

import cz.cvut.fel.pages.MainPage;
import cz.cvut.fel.pages.RoutePage;
import cz.cvut.fel.utils.SeleniumUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MainFunctionalityTests extends SeleniumBaseTest {

    @Test
    public void mainMenuTicketsTestSelenium() {
        webDriver.get(BASE_URL);
        String expURL = BASE_URL + "tickets/search?currency=11&departure=2024-06-30&destination=808&direction=outbound&mode=purchase&origin=200&pax%5B1%5D=1&return=&trip=abroad";

        MainPage mainPage = new MainPage(webDriver);
        SeleniumUtils.sleep(1000);

        mainPage.searchRouteFromAlytusToLida();
        SeleniumUtils.sleep(1500);

        Assertions.assertEquals(expURL, webDriver.getCurrentUrl());
    }

    @Test
    public void waysPageMenuTicketsTestSelenium() {
        webDriver.get(BASE_URL + "offers/riga-warsaw?month=6");
        String expURL = BASE_URL + "tickets/search?currency=11&departure=2024-06-30&destination=100&direction=outbound&mode=purchase&origin=1&pax%5B1%5D=1&return=&trip=abroad";

        RoutePage routePage = new RoutePage(webDriver);
        SeleniumUtils.sleep(1000);

        routePage.findRouteTickets();
        SeleniumUtils.sleep(2000);

        Assertions.assertEquals(expURL, webDriver.getCurrentUrl());
    }

    @Test
    public void changeLanguageAndCurrencyTestSelenium() {
        webDriver.get(BASE_URL);

        String expectingURL = BASE_URL + "bg-BG";
        MainPage mainPage = new MainPage(webDriver);
        SeleniumUtils.sleep(1000);

        mainPage.clickChangeLanguageButton();
        mainPage.clickBulgarianCountryLanguageAndCurrency();

        Assertions.assertEquals(expectingURL, webDriver.getCurrentUrl());
    }
}
