package cz.cvut.fel;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Objects;

public class SeleniumBaseTest {
    protected static WebDriver webDriver;
    protected static final String BASE_URL = "https://ecolines.net/";

    @BeforeEach
    public void setUp() {
        WebDriverManager
                .firefoxdriver().setup();
        webDriver = new FirefoxDriver();
    }

    @AfterEach
    public void tearDown() {
        if (Objects.nonNull(webDriver)) {
            webDriver.quit();
        }
    }
}
