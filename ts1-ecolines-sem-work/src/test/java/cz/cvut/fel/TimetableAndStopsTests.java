package cz.cvut.fel;

import cz.cvut.fel.pages.BusStopsPage;
import cz.cvut.fel.pages.TimetablePage;
import cz.cvut.fel.utils.SeleniumUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TimetableAndStopsTests extends SeleniumBaseTest {

    @Test
    public void timetableInfoPanelTestSelenium() {
        webDriver.get(BASE_URL + "timetable");

        String expURL = BASE_URL + "timetable?destination_stop_id=808&origin_stop_id=200";
        String expTitle = "Bus schedule Alytus - Lida";
        TimetablePage timetablePage = new TimetablePage(webDriver);

        timetablePage.selectTimetableFromAlytusToLida();

        SeleniumUtils.sleep(2000);
        Assertions.assertEquals(expURL, webDriver.getCurrentUrl());
        Assertions.assertEquals(expTitle, timetablePage.getTitleText());
    }

    @Test
    public void busStopListTestSelenium() {
        webDriver.get(BASE_URL + "bus-stops");
        List<String> expectedTitles = List.of("Baranovichi", "Brest", "Lida", "Minsk (Centralniy)", "Orsha", "Vitebsk");

        BusStopsPage busStopsPage = new BusStopsPage(webDriver);
        SeleniumUtils.sleep(1500);

        busStopsPage.clickBelarusCardTitle();

        Assertions.assertTrue(SeleniumUtils.isDisplayed(webDriver, BusStopsPage.cardAddresses));

        boolean titlesMatch = busStopsPage.verifyCardTitles(expectedTitles);
        Assertions.assertTrue(titlesMatch);
    }
}
